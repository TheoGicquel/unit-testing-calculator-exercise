using System;
using Xunit;

using CalculatriceConsole;

namespace CalculatriceTest
{
    public class OperationTest
    {
        [Fact]
        public void TestMultiplication()
        {
            Assert.Equal(4,Program.Multiplication(2,2));
        }

        [Fact]
        public void TestMultiplicationfacteur()
        {
            Assert.Equal(21,Program.Multiplication(7,3));
        }
    }
}
