$ mkdir CalculatriceProjet
$ cd CalculatriceProjet
$ dotnet new sln
Création du projet CalculatriceConsole, idem d'abord la création d'un répertoire qui contiendra notre projet console. (Notez que CalculatriceConsole est un sous répertoire de CalculatriceProjet)

$ mkdir CalculatriceConsole
$ cd CalculatriceConsole
$ dotnet new console
$ cd ..
$ dotnet sln add CalculatriceConsole/CalculatriceConsole.csproj
Une fois notre projet créé, il nous faut créer le projet qui contiendra nos tests unitaires.

$ mkdir CalculatriceTest
$ cd CalculatriceTest
$ dotnet new xunit
$ dotnet add reference ../CalculatriceConsole/CalculatriceConsole.csproj
$ cd ..
$ dotnet sln add CalculatriceTest/CalculatriceTest.csproj
Félicitations ! Nous venons de créer notre première solution incluant un projet console et un projet de tests unitaires.

Approche TDD
Le but de ce TP est de créer plusieurs fonctions qui permettront de faire les opérations mathématique suivantes:

addition
soustraction
multiplication
division
Nous allons voir ensemble comment créer la suite de tests pour la multiplication.

Création de la signature
Dans notre fichier Program.cs, nous allons créer la signature de la méthode multiplication:

using System;

namespace CalculatriceConsole
{
    public class Program
    {
        public static int Multiplication(int facteur_gauche, int facteur_droite)
        {
            throw new NotImplementedException();
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
Nous n'avons que créer la signature de la méthode Multiplication, en effet celle-ci renvoie une exception NotImplementedException qui signifie que la méthode .. n'est pas implémentée.

Création de notre premier tests
Attention la syntaxe des tests unitaires est différentes entre .Net Framework et .Net Core, pensez donc à faire bien attention à quels code source vous recopiez !

Nous allons ajouter à notre fichier OperationTests.cs de notre projet de tests (CalculatriceTests) un premier test:

OperationTest.cs sous .Net Framework:

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using CalculatriceConsole;

namespace CalculatriceConsoleTests
{
    [TestClass]
    public class OperationTest
    {
        [TestMethod]
        public void TestMultiplication()
        {
            Assert.AreEqual(4, Program.Multiplication(2, 2));
        }
    }
}
OperationTest.cs sous .Net Core:

using System;
using Xunit;

using CalculatriceConsole;

namespace CalculatriceTest
{
    public class OperationTest
    {
        [Fact]
        public void TestMultiplication()
        {
            Assert.Equal(4, Program.Multiplication(2, 2));
        }
    }
}
Nous pouvons éxécuter notre premier test,

pour ce faire via Visual Studio 2017 en utilisant le menu Test > Exécuter > Tous les tests
via .Net Core et visual Studio Code, dans un terminal (situé dans le répertoire de la solution):
$ cd CalculatriceTest
$ dotnet test
Que constatez vous ?
Comprenez vous cette erreur ?
Implémentation de la méthode multiplication
Nous venons de respecter les deux premières étapes du TDD, créer un test et vérifier que celui-ci est en erreur étant donné qu'il n'y pas de code source. Maintenant nous allons écrire juste assez de code pour que celui-ci passe.

using System;

namespace CalculatriceConsole
{
    public class Program
    {
        public static int Multiplication(int facteur_gauche, int facteur_droite)
        {
            return facteur_gauche + facteur_droite;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
Non non ce code ne contient aucune erreur, ne modifiez pas la ligne du corp de la méthode Multiplication.

Et si on relance notre test maintenant ?
Pensez vous que notre jeu de test est suffisament fiable ?
Complexifions notre jeu de test
Et si nous ajoutions une assertion à notre méthode TestMultiplication pour vérifier que le produit de 7 par 3 est égale à 21 ?
Que se passe t'il lors de la relance de notre test ?
Pouvez-vous corriger cette erreur ?
Que se passe t'il lors de la relance de notre test ?
Félicitations, vous venez de découvrir les tests unitaires et le TDD ! Désormais vous devrez utiliser cette technique de développement pour chaque TD !